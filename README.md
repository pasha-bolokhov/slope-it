# SlopeIt
A *Google Sheets* add-on for analysis and linear fits of numerical data &mdash; [SlopeIt](https://gsuite.google.com/marketplace/app/slopeit/1088613043056)

This is the official source code repository of the app

Deployment to G Suite Marketplace is done using [Clasp](https://github.com/google/clasp)
```shell
$ clasp pull
....
$ clasp push
$ clasp version "Add an interactive analysis capability"
```
