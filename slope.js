/**
 * LICENSE
 *
 * Copyright 2019 (C) Pasha Bolokhov <pasha.bolokhov@gmail.com>
 *
 */


/*
 * A function that is called upon the Add-On installation
 */
function onInstall(e) {
  onOpen(e);
}

/*
 * A function that creates a dedicated menu item
 */
function onOpen(e) { 
  var ui = SpreadsheetApp.getUi();
  ui.createMenu('SlopeIt')
      .addItem('Find slope', 'calculateSlope_')
      .addSeparator()
      .addItem('How to use', 'displayHelp_')
      .addToUi();
}

/**
 * A function that is called by the client to refresh the slope information
 */
function refreshSlope() {
  calculateSlope_();
}

/**
 * A function that is called by the client to show the graph
 */
function showChart() {
  // restore the saved quantities and properties
  var up = PropertiesService.getUserProperties();
  var meta = JSON.parse(up.getProperty('meta'));
  drawChart_(meta);
}

/**
 * A function that calculates the slope from the data from the user selection
 */
function calculateSlope_() {
  var sheet = SpreadsheetApp.getActiveSheet();
  var selection = sheet.getSelection();
  var headerX = "X";
  var headerY = "Y";
  
  if (selection === null) {
    notEnoughSelectionMessage_();
    return;
  }
  // !!!! Check that there is a selection and it has a reasonable length and width
  
  var range = selection.getActiveRange();
  
  data = range.getValues();
  if (data.length < 2 || data[0].length < 2) {
    notEnoughSelectionMessage_();
    return;
  }

  // if the first row is non-numeric, treat it as the headers
  if (isNaN(data[0][0]) || isNaN(data[0][1])) {
    headerX = data[0][0];
    headerY = data[0][1];
    range = range.offset(1, 0, data.length - 1);	// shrink the data range
    data = range.getValues();
  }
  
  // !!!! Check that the data are numbers
  var slope_xy = findSlopeXYWithNoDataErrors_(data);
  var slope_yx = findSlopeYXWithNoDataErrors_(data);
  var slope_error = findSlopeErrorWithNoDataErrors_(data, slope_xy);
  var excel_slope = findExcelSlope_(data);
  if (isNaN(slope_xy) ||isNaN(slope_yx) || isNaN(excel_slope)) {
    nonNumericSelectionMessage_();
  } else {
    var slope_min = slope_xy - slope_error;
    var slope_max = slope_xy + slope_error;

    // save the values that we have determined as metadata
    var up = PropertiesService.getUserProperties();
    meta = {
      'range': range.getA1Notation(),
      'headerX': headerX,
      'headerY': headerY,
      'slope_xy': slope_xy,
      'slope_yx': slope_yx,
      'slope_error': slope_error,
      'slope_min': slope_min,
      'slope_max': slope_max,
      'excel_slope': excel_slope,
    };
    up.setProperty('meta', JSON.stringify(meta));

    // report the slope
    reportSlope_(meta);
  }
}

/**
 * A function that calculates the slope assuming zero intercept assuming X errors negligible
 *
 * @param {List} data    data table consisting of exactly two columns
 */
function findSlopeXYWithNoDataErrors_(data) {
  var sum_xx = 0.0;
  var sum_xy = 0.0;
  
  for (var k = 0; k < data.length; k++) {
    if (isNaN(data[k][0]) || isNaN(data[k][1])) {
      continue;
    }
    sum_xx += data[k][0] * data[k][0];
    sum_xy += data[k][0] * data[k][1];
  }
  if (isNaN(sum_xy) || isNaN(sum_xx)) {
    return NaN;
  }
  var slope = sum_xy / sum_xx;

  return slope;
}

/**
 * A function that calculates the slope assuming zero intercept assuming Y errors negligible
 *
 * @param {List} data    data table consisting of exactly two columns
 */
function findSlopeYXWithNoDataErrors_(data) {
  var sum_yy = 0.0;
  var sum_yx = 0.0;
  
  for (var k = 0; k < data.length; k++) {
    if (isNaN(data[k][0]) || isNaN(data[k][1])) {
      continue;
    }
    sum_yy += data[k][1] * data[k][1];
    sum_yx += data[k][1] * data[k][0];
  }
  if (isNaN(sum_yx) || isNaN(sum_yy)) {
    return NaN;
  }
  var slope = sum_yy / sum_yx;

  return slope;
}

/**
 * A function that calculates the slope the same way Excel does
 *
 * @param {List} data    data table consisting of exactly two columns
 */
function findExcelSlope_(data) {
  var sum_xx = 0.0;
  var sum_xy = 0.0;
  
  /* find the mean values */
  var mean_x = 0.0;
  var mean_y = 0.0;
  for (var j = 0; j < data.length; j++) {
    if (isNaN(data[j][0]) || isNaN(data[j][1])) {
      continue;
    }
    mean_x += data[j][0];
    mean_y += data[j][1];
  }
  mean_x /= data.length;
  mean_y /= data.length;
  
  /* calculate the slope */
  for (var k = 0; k < data.length; k++) {
    if (isNaN(data[k][0]) || isNaN(data[k][1])) {
      continue;
    }
    sum_xx += (data[k][0] - mean_x) * (data[k][0] - mean_x);
    sum_xy += (data[k][0] - mean_x) * (data[k][1] - mean_y);
  }
  if (isNaN(sum_xy) || isNaN(sum_xx)) {
    return NaN;
  }
  var slope = sum_xy / sum_xx;

  return slope;
}

/**
 * A function that calculates the error for the slope, assuming no errors in data
 *
 * @param {List}	data	data table consisting of exactly two columns
 * @param {Number}	slope	slope that has already been found
 */
function findSlopeErrorWithNoDataErrors_(data, slope) {
  var sum_xx = 0.0;
  var sum_xy = 0.0;
  var N = data.length;
  var factor = 0.0;

  if (isNaN(slope)) {
    return NaN;
  }
  for (var k = 0; k < data.length; k++) {
    if (isNaN(data[k][0]) || isNaN(data[k][1])) {
      continue;
    }
    sum_xx += data[k][0] * data[k][0];
    var term = (data[k][1] - slope * data[k][0]);
    factor += term * term;
  }
  factor /= N - 1;
  if (isNaN(sum_xx) || isNaN(factor)) {
    return NaN;
  }
  return Math.sqrt(factor / sum_xx);
}

/**
 * A function that informs the user that not enough data has been selected
 */
function notEnoughSelectionMessage_() {
  ui = SpreadsheetApp.getUi();
  ui.alert('Hold it', 'Select two or more data columns', ui.ButtonSet.OK);
}

/**
 * A function that informs the user that the data are not numbers
 */
function nonNumericSelectionMessage_() {
  ui = SpreadsheetApp.getUi();
  ui.alert('Heads up', 'Make sure to select numeric data', ui.ButtonSet.OK);
}

/**
 * A function that informs the user of the results
 *
 * @param {object} meta    the metadata from the calculations
 */
function reportSlope_(meta) {
  // !!!! Use a good number format
  ui = SpreadsheetApp.getUi();
  tmpl = HtmlService
    .createTemplateFromFile('report.html');
  tmpl.data = { "slope_mid": Utilities.formatString("%1.4f", meta.slope_xy),
                "slope_error": Utilities.formatString("%1.4f", meta.slope_error),
                "slope_min": Utilities.formatString("%1.4f", meta.slope_min),
                "slope_max": Utilities.formatString("%1.4f", meta.slope_max),
                "excel_slope": Utilities.formatString("%1.4f", meta.excel_slope) };
  html = tmpl.evaluate().setTitle('Slope');
  ui.showSidebar(html);
}

/**
 * A function that shows the graph of the data
 *
 * @param {object} meta		the calculated values and metadata
 */
function drawChart_(meta) {
  // Open a dialog with the chart
  var sheet = SpreadsheetApp.getActiveSheet();
  var range = sheet.getRange(meta.range);
  var tmpl = HtmlService.createTemplateFromFile('chart.html');
  tmpl.meta = meta;
  tmpl.meta["data"] = range.getValues();
  var html = tmpl.evaluate().setTitle('Chart').setWidth(840).setHeight(620);
  SpreadsheetApp.getUi().showModelessDialog(html, 'Chart');
}

/**
 * A function that shows help to the user
 */
function displayHelp_() {
  ui = SpreadsheetApp.getUi();
  html = HtmlService.createHtmlOutputFromFile('help.html').setTitle('Help');
  ui.showSidebar(html);
}
